// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
// 'starter.controllers' is found in controllers.js
// angular.module('boosterapp', ['ionic', 'ngCordova', 'boosterapp.controllers', 'boosterapp.directives'])
angular.module('boosterapp', ['ionic', 'ngCordova', 'boosterapp.utility', 'boosterapp.services', 'boosterapp.controllers', 'boosterapp.directives'])

.run(function($ionicPlatform, Utility, $state, $rootScope) {

  // $rs = $rootScope;
  // $rs.devMode = true;
  // $rs.debug = 19;

  //.run(function($ionicPlatform) {

  // header('Access-Control-Allow-Origin: *');
  // header('Access-Control-Allow-Methods: GET, POST');

  $ionicPlatform.ready(function() {
    // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
    // for form inputs)
    if (window.cordova && window.cordova.plugins.Keyboard) {
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
    }
    if (window.StatusBar) {
      // org.apache.cordova.statusbar required
      StatusBar.styleDefault();
    }

    //if ($rs.debug > 3) console.log('run:doPromiseStartAppOnce');
    //Utility.doPromiseStartAppOnce();

  });
})

.config(function($stateProvider, $urlRouterProvider) {
  $stateProvider

    .state('app', {
      url: "/app",
      abstract: true,
      templateUrl: "templates/menu.html",
      controller: 'AppCtrl'
    })
    // .state('app.feed', {
    //   url: "/feed",
    //   views: {
    //     'menuContent': {
    //       templateUrl: "templates/infinite.tpl.html",
    //       controller: 'InfiniteCtrl'
    //     }
    //   }
    // })
    // .state('app.scroll', {
    //   url: "/scroll",
    //   views: {
    //     'menuContent': {
    //       templateUrl: "templates/ba-scroll.tpl.html",
    //       controller: 'BaScrollCtrl'
    //     }
    //   }
    // })

  .state('app.goals-world', {
      url: "/goals-world",
      views: {
        'menuContent': {
          templateUrl: "templates/ba-goals.tpl.html",
          controller: 'HomeCtrl'
        }
      }
    })
    .state('app.likes', {
      url: "/likes/:selfieId",
      views: {
        'menuContent': {
          templateUrl: "templates/ba-likes-list.html",
          controller: 'HomeCtrl'
        }
      }
    })
    .state('app.goals-friends', {
      url: "/goals-friends",
      views: {
        'menuContent': {
          templateUrl: "templates/ba-goals.tpl.html",
          controller: 'HomeCtrl'
        }
      }
    })
    .state('app.goals-my', {
      url: "/goals-my",
      views: {
        'menuContent': {
          templateUrl: "templates/ba-goals.tpl.html",
          controller: "HomeCtrl"
        }
      }
    })
    .state('app.goals-boostees', {
      url: "/goals-boostees",
      views: {
        'menuContent': {
          templateUrl: "templates/ba-goals.tpl.html",
          controller: 'HomeCtrl'
        }
      }
    })
    .state('app.notifications', {
      url: "/notifications",
      views: {
        'menuContent': {
          templateUrl: "templates/notifications.tpl.html",
          controller: 'NotificationsCtrl'
        }
      }
    })
    .state('app.new-goal-summary', {
      url: "/new-goal-summary",
      views: {
        'menuContent': {
          templateUrl: "templates/new-goal-summary.html",
          controller: 'NewGoalSummaryCtrl'
        }
      }
    })
    .state('app.profile', {
      url: "/profile",
      views: {
        'menuContent': {
          templateUrl: "templates/profile.tpl.html",
          controller: 'ProfileCtrl'
        }
      }
    })
    .state('app.invite', {
      url: "/invite",
      views: {
        'menuContent': {
          templateUrl: "templates/invite.tpl.html",
          controller: 'InviteCtrl'
        }
      }
    })
    .state('app.share', {
      url: "/share",
      views: {
        'menuContent': {
          templateUrl: "templates/share.html"
        }
      }
    })
    .state('app.about', {
      url: "/about",
      views: {
        'menuContent': {
          templateUrl: "templates/about.tpl.html",
          controller: 'AboutCtrl'
        }
      }
    });
  // if none of the above states are matched, use this as the fallback
  $urlRouterProvider.otherwise('/app/goals-my');
});
