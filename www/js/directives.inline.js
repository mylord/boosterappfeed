angular.module('boosterapp.directives', [])

.directive('tabsBottom', function() {
  return {
    restrict: 'E',
    scope: {},
    link: function(scope) {},
    template: '<ion-tabs>\
        <ion-tab title="Feed" icon-on="tab-feed-on" icon-off="tab-feed-off" href="#/app/search">\
        </ion-tab>\
        <ion-tab title="MyGoals" icon-on="tab-mygoals-on" icon-off="tab-mygoals-off" href="#/app/browse">\
        </ion-tab>\
    </ion-tabs>'
  }
});
