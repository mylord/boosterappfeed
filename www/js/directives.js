angular.module('boosterapp.directives', []);

angular.module('boosterapp.directives')

.directive('baLikeRow', function() {
    return {
      restrict: 'AE',
      templateUrl: 'templates/ba-like-row.tpl.html'
    }
  })
  .directive('baLikesList', function() {
    return {
      restrict: 'AE',
      templateUrl: 'templates/ba-likes-list.tpl.html',
      controller: function($scope, $state, $stateParams, Booster, Utility) {
        $scope.likers = [];
        console.log('$stateParams=' + Utility.jStr($stateParams));
        var selfieId = $stateParams.selfieId;
        var fbToken = Utility.getFbToken();
        Booster.getSelfieLikes(selfieId, fbToken, -1, -1).then(function(result) {
          $scope.likers = result.data;
          console.log('$scope.likers=' + Utility.jStr($scope.likers));
        }).catch(function(exc) {
          console.log('baLikes:getSelfieLikes:exc=' + exc);
        })
      }
    }
  })
  .directive('baGoal', function() {
    return {
      restrict: 'AE',
      // link: function(scope, element, attrs) {
      //   scope.getContentUrl = function() {
      //     return 'templates/ba-goal-completed.tpl.html';
      //   }
      // },
      // template: '<div ng-include="getContentUrl()"></div>'
      templateUrl: 'templates/ba-goal.tpl.html'
    }
  })
  .directive('baSharePrivacy', function() {
    return {
      restrict: 'AE',
      link: function($scope, $element, $attrs) {},
      templateUrl: 'templates/ba-share-privacy.tpl.html',
      controller: function($scope) {
        //alert('baSharePrivacy');
        $scope.isPublic = true;
        $scope.setPublic = function(b) {
          $scope.isPublic = b;
          // alert('isPublic:b=' + b);
        }
        $scope.getWorldClass = function() {
          // console.log('getWorldClass');
          var classes = {};
          if ($scope.isPublic) {
            classes['worldSectionOn'] = true;
          } else {
            classes['worldSectionOff'] = true;
          }
          // console.log('getWorldClass=' + JSON.stringify(classes, undefined, 2));
          return classes;
        }
        $scope.getFriendsClass = function() {
          // console.log('getFriendsClass');
          var classes = {};
          if (!$scope.isPublic) {
            classes['friendsSectionOn'] = true;
          } else {
            classes['friendsSectionOff'] = true;
          }
          // console.log('getFriendsClass=' + JSON.stringify(classes, undefined, 2));
          return classes;
        }
      }
    }
  })
  .directive('tabsBottom', function() {
    return {
      restrict: 'AE',
      link: function($scope, $element, $attrs) {},
      templateUrl: 'templates/tabs-bottom.tpl.html'
    }
  })
  .directive('newGoalSummary', function($ionicGesture) {
    return {
      restrict: 'AE',
      link: function($scope, $element, $attrs) {},
      templateUrl: 'templates/new-goal-summary.tpl.html',
      controller: function($scope, $state, $stateParams, Booster) {
        var state = $state.$current;
        if (state == 'app.new-goal-summary') {}
      }
    }
  })
  .directive('addBoostBtn', function() {
    return {
      restrict: 'AE',
      link: function($scope, $element, $attrs) {},
      templateUrl: 'templates/add-boost-btn.tpl.html',
      controller: function($scope, $state, $stateParams, Booster) {
        var state = $state.$current;
        $scope.getClass = function() {
          var classes = {};
          console.log('state' + state);
          console.log('$scope.showBoostBtn' + $scope.showBoostBtn);
          if ($scope.showBoostBtn) {
            // if (state == 'app.new-goal-summary-finished') {
            classes['add-boost-btn-on'] = true;
          } else {
            classes['add-boost-btn-off'] = true;
          }
          console.log('classes=' + JSON.stringify(classes));
          return classes;
        }
      }
    }
  })
