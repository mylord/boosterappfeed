'use strict';

angular.module('boosterapp.services', ['ionic'])

.service('Booster', function($http, $cordovaDevice, $rootScope) {
  //var baseUrl = "http://devb.theboosterapp.com/api/";
  var baseUrl = "http://mapapin.com/api/";
  //var baseUrl = "http://ws.theboosterapp.com/api/";

  this.LoginOrRegister = function(FbToken) {
    return $http({
      method: 'GET',
      url: baseUrl + 'User/LoginOrRegister?FbToken=' + FbToken
        // data: {
        //   FbToken: FbToken
        // }
    });
  }

  // debug level
  // 0-10 log importance -- error = 0
  // *10 if in a loop
  // *10 if its a large block (eg JSON)
  // *10 if its a block of blocks
  this.d = function() {
    return 8;
  }

  //use for e.g. FB Graph to get friends
  this.getUrl = function(theUrl) {
    return $http({
      method: 'GET',
      url: theUrl
    });
  }

  this.updateDeviceId = function(userId, deviceId) {

    var device = ionic.Platform.device();
    var uuid = device.uuid;
    window.localStorage.setItem("uuid", uuid);
    var platform = ionic.Platform.device().platform;
    var qString = "userId=" + userId + "&deviceId=" + deviceId + "&platform=" + platform + '&uuid=' + uuid; //Note: for android "A", Ios "I";
    var theUrl = baseUrl + "User/UpdateDeviceId?" + qString;
    console.log('updateDeviceId:theUrl=' + theUrl);
    return $http({
      method: 'GET',
      url: theUrl
    });

  }

  this.likeSelfie = function(selfieId, like, fbToken) {
    return $http({
      method: 'POST',
      url: baseUrl + "Boost/LikeSelfie",
      data: {
        selfieId: selfieId,
        LikeStatus: like,
        FbToken: fbToken
      }
    });
  };

  /*
  this.getProfile = function(userId) {
    console.log('services.getProfile:userId=' + userId);
    var qString = "userId=" + userId;
    return $http({
      method: 'GET',
      url: baseUrl + "User/getProfile?" + qString
    });
  }

  this.getProfileByFbId = function(fbId) {
    console.log('services.getProfileByFbId:fbId=' + fbId);
    var qString = "fbUserId=" + fbId;
    return $http({
      method: 'GET',
      url: baseUrl + "User/GetProfileByFbUserId?" + qString
    });
  }
  */

  this.getUpcomingBoost = function(userId, topN, skip) {
    return $http({
      method: 'GET',
      url: baseUrl + "Boost/GetUpcommingBoost?userId=" + userId + "&topN=" + top + "&skip=" + skip,
    });
  }

  /*
  this.getRequestBoostCount = function(userId) {
    return $http({
      method: 'GET',
      url: baseUrl + "Boost/GetBoostRequestCount?userId=" + userId
    });
  }

  this.getRequestBoost = function(userId) {
    return $http({
      method: 'GET',
      url: baseUrl + "Boost/GetBoostRequest?userId=" + userId
    });
  }

  this.acceptBoostRequest = function(boostID) {
    return $http({
      method: 'GET',
      url: baseUrl + "Boost/AcceptBoost?boostId=" + boostID
    });
  }

  this.declineBoostRequest = function(boostId) {
    return $http({
      method: 'GET',
      url: baseUrl + "Boost/RejectBoost?boostId=" + boostId
    });
  }
  */

  this.getSelfieLikes = function(SelfieId, fbToken, topN, skipN) {
    return $http({
      method: 'GET',
      url: baseUrl + "Boost/GetSelfieLikes?SelfieId=" + SelfieId + "&FbToken=" + fbToken + "&topN=" + topN + "&skipN=" + skipN
    });
  }

  this.getAllBoosts = function(who, getUpcoming, withImage, top, skip, fbToken) {
    return $http({
      method: 'GET',
      url: baseUrl + "Boost/getAllBoosts?who=" + who + "&getUpcomingBoost=" + getUpcoming + "&withImage=" + withImage + "&top=" + top + "&skip=" + skip + "&FbToken=" + fbToken
    });
  }

  this.getBoostSelfies = function(userId, withImage, top, skip, fbToken) {
    return $http({
      method: 'GET',
      url: baseUrl + "Boost/GetBoostSelfies?userId=" + userId + "&withImage=" + withImage + "&top=" + top + "&skip=" + skip
    });
  }

  this.getCompletedBoost = function(userId, top, skip) {
    return $http({
      method: 'GET',
      url: baseUrl + "Boost/GetCompletedBoost?userId=" + userId + "&top=" + top + "&skip=" + skip
    });
  }

  this.addBoost = function(fromUserId, toUserId, date, time, title, alert) {
    return $http({
      method: 'POST',
      url: baseUrl + "Boost/CreateBoostRequest",
      data: {
        UserId: fromUserId,
        ToFbUserId: toUserId,
        Title: title,
        EventDate: date,
        TzOffset: time,
        Alert: alert
      }
    });
  }

  this.getBoostDetails = function(boostId) {
    return $http({
      method: 'GET',
      url: baseUrl + "Boost/GetBoostDetail?boostId=" + boostId
    });
  }

  this.markNotfRead = function(userId) {
    return $http({
      method: 'GET',
      url: baseUrl + "Boost/MarkNotfRead?userId=" + userId
    });
  }

  this.updateBoost = function(boostId, fromUserId, toUserId, dateTime, alert) {
    return $http({
      method: 'POST',
      url: baseUrl + "AddBoost",
      data: {
        boostId: boostId,
        fromUserId: fromUserId,
        toUserId: toUserId,
        dateTime: dateTime,
        alert: alert
      }
    });
  }

  /*
  this.addBoostReview = function(selfieId, comment, isLovedIt) {
    return $http({
      method: 'POST',
      url: baseUrl + "Boost/AddSelfieReview",
      data: {
        BoostSelfieId: selfieId,
        Comment: comment,
        LovedIt: isLovedIt
      }
    });
  }
  */

  this.sendSelfie = function(boostId, comment, imageData) {
    return $http({
      method: 'POST',
      url: baseUrl + "Boost/SendSelfie",
      data: {
        BoostId: boostId,
        Comment: comment,
        Image: imageData,
      }
    });
  }

  this.getSelfie = function(boostId) {
    return $http({
      method: 'GET',
      url: baseUrl + "Boost/GetSelfieDetails?boostId=" + boostId
    });
  }

  this.getNotification = function(userId) {
    return $http({
      method: 'GET',
      url: baseUrl + "Boost/GetNotficatios?userId=" + userId
    });
  }

  this.sendPoke = function(boostId) {
    var theUrl = baseUrl + 'Boost/SendPoke?boostId=' + boostId;
    return $http({
      method: 'GET',
      url: theUrl
    });
  }

  this.getDefaultBoosters = function() {
    return $http({
      method: 'GET',
      url: baseUrl + "User/GetDefaultBooster"
    });
  }


})

.service('PushNotifications', function(Utility, $location, $ionicPlatform, $cordovaToast, $rootScope, $state) {

  var pushPlugin = null;

  this.initialize = function() {

    pushPlugin = window.plugins.pushNotification;

    var dp = Utility.getDevicePlatform();
    if (dp == 'android' || dp == 'Android' || dp == "amazon-fireos") {
      pushPlugin.register(
        function(result) {
          console.log('PushNotifications:initialize:1:result=' + result);
        },
        function(result) {
          alert('PushNotifications:initialize:2:result=' + result);
          console.log('PushNotifications:initialize:2:result=' + JSON.stringify(result));
        }, {
          "senderID": "220739435041",
          "ecb": "onNotificationGCM"
        });

    } else if (dp == 'iOS') {
      pushPlugin.register(
        function(result) {
          console.log('pushPlugin.register:1:result=' + JSON.stringify(result));
          Utility.setDeviceId(result)
        },
        function(result) {
          console.log('pushPlugin.register:2:result=' + JSON.stringify(result));
        }, {
          "badge": "true",
          "sound": "true",
          "alert": "true",
          "ecb": "onNotificationAPN"
        });
    }

    // notifications for Android
    window.onNotificationGCM = function(e) {
      window.boosterNotification = e;
      switch (e.event) {
        case 'registered':
          if (e.regid.length > 0) {
            Utility.setDeviceId(e.regid);
          }
          break;

        case 'message':
          // if this flag is set, this notification happened while we were in the foreground.
          // you might want to play a sound to get the user's attention, throw up a dialog, etc.
          if (e.foreground) {
            // on Android soundname is outside the payload. 
            // On Amazon FireOS all custom attributes are contained within payload
            var soundfile = e.soundname || e.payload.sound;
            // if the notification contains a soundname, play it.
            //var my_media = new Media("/android_asset/www/"+ soundfile);
            //my_media.play();
          } else { // otherwise we were launched because the user touched a notification in the notification tray.
            if (e.coldstart) {
              //
            } else {
              //
            }
          }

          Utility.sendToScreen(e.payload);

          /*
        var msg = e.payload.message.replace(/<b>/g, "")
        msg = msg.replace(/<\/b>/g, "");
        $cordovaToast.showShortCenter(msg).then(function(success) {
                        //$state.go('app.upcoming');
                        Utility.updateNotifications();
                      }, function (error) {
                        // error
                  }
                );
        */

          //if(Utility.debug>0) console.log(e.payload.message); 
          //Only works for GCM
          // e.payload.msgcnt + '</li>');
          //Only works on Amazon Fire OS
          // e.payload.timeStamp
          break;

        case 'error':
          //e.msg 
          break;

        default:
          // Unknown
          break;
      }

    };

    // notifications for iOS
    window.onNotificationAPN = function(result) {
      //if (Utility.debug > 3) console.log('onNotificationAPN:result='+JSON.stringify(result));
      //if(Utility.debug>0) console.log('onNotificationAPN:result='+JSON.stringify(result));

      /*
      if ( event.alert )
        {
            //navigator.notification.alert(event.alert);
        }

        if ( event.sound )
        {
            //var snd = new Media(event.sound);
            //snd.play();
        }

        if ( event.badge )
        {
            //.setApplicationIconBadgeNumber(successHandler, errorHandler, event.badge);
        }
        */

      Utility.sendToScreen(result);

    };

  };

  Utility.handleNotification = function(buttonIndex) {
    console.log('handleNotification:buttonIndex=' + buttonIndex);

    if (buttonIndex == 2) // if CANCEL, return
    {
      return;
    }

    // else, goto screen
    if (Utility.varIsSet(Utility.isAppStarted)) {
      if (Utility.varIsSet(Utility.pushState)) {
        if (Utility.varIsSet(Utility.pushTarget)) {
          $state.go(Utility.pushState, {
            boostId: Utility.pushTarget
          });
        } else {
          $state.go(Utility.pushState);
        }
      }
    }
  }

  Utility.sendToScreen = function(result) {
    console.log('new sendToScreen:result=' + JSON.stringify(result));
    var nType = result.nType;
    var targetId = result.targetId;
    var firstName = result.firstName;
    var boostName = result.boostName;
    console.log('sendToScreen:nType=' + nType);
    console.log('sendToScreen:targetId=' + targetId);
    console.log('sendToScreen:firstName=' + firstName);
    console.log('sendToScreen:boostName=' + boostName);

    var msg;
    if (nType == "AcceptBoost") {
      //r = window.confirm(''+firstName + ' accepted your request for '+boostName+'! Check it now?');
      msg = firstName + ' accepted your request for ' + boostName + '! Check it now?';
      //Utility.pushState = 'app.upcoming';
      Utility.pushState = 'app.selfie';
      Utility.pushTarget = result.targetId;
      //$location.path("/app/selfie/" + item.TargetId + "/true");
    } else if (nType == "Boost") {
      msg = firstName + ' Boosted you to ' + boostName + '! Check it now?';
      //Utility.pushState = 'app.upcoming';
      Utility.pushState = 'app.selfie';
      Utility.pushTarget = result.targetId;
      //$location.path("/app/selfie/" + item.TargetId + "/true");
    } else if (nType == "RejectBoost") {
      msg = firstName + ' rejected your request for ' + boostName + '! Check it now?';
      Utility.pushState = 'app.upcoming';
    } else if (nType == "BoostRequest") {
      msg = firstName + ' requested a boost to ' + boostName + '! Check it now?';
      Utility.pushState = 'app.upcoming';
    } else if (nType == "Selfie") {
      msg = firstName + ' sent you a selfi for ' + boostName + '! Check it now?';
      // Utility.pushState = 'app.completed';
      Utility.pushState = 'app.review';
      Utility.pushTarget = result.targetId;
      //$location.path("/app/review/" + item.TargetId + "/true");
    } else if (nType == "Review") {
      msg = firstName + ' sent you a review for ' + boostName + '! Check it now?';
      Utility.pushState = 'app.completedreview';
      Utility.pushTarget = result.targetId;
      //$location.path("/app/creview/" + item.TargetId + "/true");
    } else if (nType == "UpdateBoost") {
      msg = firstName + ' send you an update for ' + boostName + '! Check it now?';
      Utility.pushTarget = result.targetId;
    } else if (nType == "Time") {
      msg = 'It is time for ' + firstName + ' to ' + boostName + '! Check it now?';
    }

    console.log("sendToScreen:Utility.pushState=" + Utility.pushState);
    console.log("sendToScreen:Utility.pushTarget=" + Utility.pushTarget);

    window.boosterNotification = result;

    navigator.notification.confirm(msg, Utility.handleNotification, nType, ["Ok", "Cancel"])

  }


});
