angular.module('boosterapp.utility', ['ionic'])

.service('Utility', function($ionicPopup, $q, $state, Booster) {

  var use_analytics = false;

  this.checkPlugin = function(var1) {
    console.log('var=' + var1);
  }

  this.popup = function(title, message) {
    console.log('popup:title=' + title + ' ;message=' + message);
    /*
     $ionicPopup.show({
         title: title,
         template: message
     });
    */
  }

  this.goToState = function(theState) {
    $state.go(theState);
  }

  this.doDelayed = function(f1, delay, args) {
    console.log('doDelayed:args=' + JSON.stringify(args));

    function sleep(millis, callback) {
      setTimeout(function() {
        callback();
      }, millis);
    }

    function foobar_cont() {
      console.log("foobar_cont");
      console.log('doDelayed:foobar_cont:args=' + JSON.stringify(args));
      f1.apply(this, args);
    };
    sleep(delay, foobar_cont);
  }

  this.fbShare = function(selfie) {
    var d = $q.defer();
    console.log('this.fbShare:selfie.UserId=' + selfie.UserId);
    this.trackEvent("fbShare");

    var message = selfie.message, //selfie.BoostTitle,
      subject = 'Booster App',
      fileOrFileArray = "data:image/png;base64," + selfie.ImageData,
      url = "http://signup.theboosterapp.com";

    var sharing = window.plugins.socialsharing;
    sharing.share(
      message,
      subject,
      fileOrFileArray,
      url,
      function(result) {
        this.trackEvent('fbShare:success');
        console.log('Sharing success: ' + JSON.stringify(result));
        d.resolve(result);
        return d.promise;
      },
      function(result) {
        this.trackError(error, "fbShare");
        console.log('Sharing error: ' + JSON.stringify(result));
        d.reject(result);
        return d.promise;
      });
    return d.promise;
  }

  //FB login button click event
  this.promiseFacebookLogin = function() {
    Utility = this;
    this.trackEvent('promiseFacebookLogin');
    console.log('promiseFacebookLogin');
    // var fbId = this.getFBUserId();
    // var fbLr = this.getFbLr();
    // console.log('promiseFacebookLogin:2:fbLr=' + this.jStr(fbLr));
    var d = $q.defer();
    var dp = this.getDevicePlatform();
    var FbToken;
    // if (dp == 'test' || (devMode && (dp == 'undefined'))) {
    if (dp == 'test') {
      // TODO: REMOVE
      console.log('promiseFacebookLogin:test||devMode:promiseFacebookLogin:set:FbToken=' + FbToken);
      this.setFbToken(FbToken);
      d.resolve(FbToken);
      return;
    }
    try {
      if (!this.varIsSet(facebookConnectPlugin)) {
        d.resolve(FbToken);
      }

    } catch (exc) {
      console.log('promiseFacebookLogin:exc=' + exc);
      console.log('promiseFacebookLogin:set:FbToken=' + FbToken);
      this.setFbToken(FbToken);
      d.resolve(FbToken);
      return;
      // facebookConnectPlugin.browserInit('625654127541709');
    }

    facebookConnectPlugin.
    login
      (
        ['public_profile', 'user_friends', 'email'],
        // SUCCESS
        function(res) {
          console.log('facebookConnectPlugin:login:success');
          Utility.trackEvent('facebookConnectPlugin.login:success');
          console.log("promiseFacebookLogin:res=" + JSON.stringify(res));
          alert('promiseFacebookLogin:success:res.authResponse.accessToken=' + res.authResponse.accessToken);
          Utility.setFbToken(res.authResponse.accessToken);
          d.resolve(res);
          return d.promise;
        },
        function(err) {
          console.log('facebookConnectPlugin.login:fail');
          Utility.trackEvent('facebookConnectPlugin.login:fail');
          var errLog = 'promiseFacebookLogin:facebookConnectPlugin:err' + err;
          console.log(errorLog);
          d.reject(errLog);
          return d.promise;
        }
      )
    console.log('promiseFacebookLogin:end');
    return d.promise;
  };

  // WARNING: BREAKS PLUGIN SOMETIMES
  this.promiseGetLoginStatus = function() {
    this.trackEvent('promiseGetLoginStatus');
    console.log('promiseGetLoginStatus');
    var d = $q.defer();
    Utility = this;

    facebookConnectPlugin.getLoginStatus(
      function(res) {
        Utility.trackEvent('facebookConnectPlugin.getLoginStatus:success');
        var FbToken;
        console.log('promiseGetLoginStatus:getLoginStatus:res=' + Utility.jStr(res));
        if (res.status + "" == "connected") {
          Utility.trackEvent('facebookConnectPlugin.getLoginStatus==connected');
          FbToken = res.authResponse.accessToken;
          if (Utility.varIsSet(FbToken)) {
            Utility.setFbToken(FbToken);
            console.log('promiseGetLoginStatus:getLoginStatus:resolve:FbToken=' + FbToken);
            d.resolve(res);
            return d.promise;
          } else {
            console.log('promiseGetLoginStatus:getLoginStatus:reject:FbToken==' + FbToken);
            d.reject();
            return d.promise;
          }
        } else {
          console.log('promiseGetLoginStatus:getLoginStatus:reject:connected==false');
          d.reject();
          return d.promise;
        }
      },
      function(err) {
        this.trackEvent('facebookConnectPlugin.getLoginStatus:err=' + err);
        console.log('facebookConnectPlugin.getLoginStatus:err=' + err);
      }
    )
    console.log('promiseGetLoginStatus:getLoginStatus:return');
    return d.promise;
  };

  /*
  this.promiseGetProfile = function() {
    console.log('promiseGetProfile');
    var d = $q.defer();
    var fbId = this.getFBUserId();
    console.log('promiseGetProfile:fbId=' + fbId);
    if (!this.varIsSet(fbId)) {
      console.log('promiseGetProfile:reject:fbId=' + fbId);
      d.reject('fbId=' + fbId);
      return d.promise;
    }
    Booster.getProfileByFbId(fbId)
      .then(function(result) {
        console.log('promiseGetProfile:getProfileByFbId:result=' + this.jStr(result));
        this.getProfileRes = result;
        if (this.varIsSet(result.data)) {
          console.log('promiseGetProfile:getProfileByFbId:resolve');
          this.setProfileFromWs(result);
          d.resolve(result);
          return d.promise;
        } else {
          console.log('promiseGetProfile:getProfileByFbId:reject');
          d.reject(result);
          /*
          console.log('promiseGetProfile:return:promiseRegister');
          return this.promiseRegister()
          .then(function(result)
          {
              console.log('promiseGetProfile:return:promiseGetProfile');
              return this.promiseGetProfile();
              //d.reject(result);
          });
          */
  /*
        }
      }).catch(function(err) {
        console.log('catch:promiseGetProfile:getProfileByFbId:reject');
        d.reject(err);
        return d.promise;
      });
    return d.promise;
  }
  */

  this.promiseLoginOrRegister = function() {
    var d = $q.defer();
    // var fbLr = this.getFbLr();
    var FbToken = this.getFbToken();
    console.log('promiseLoginOrRegister:FbToken=' + FbToken);
    if (!this.varIsSet(FbToken)) {
      d.reject(FbToken);
      return d.promise;
    }
    Booster.LoginOrRegister(FbToken)
      .then(function(result) {
          console.log('promiseLoginOrRegister:LoginOrRegister:result=' + JSON.stringify(result));
          if (this.varIsSet(result.data.UserId)) {
            var data = result.data;
            console.log("LoginOrRegister:data.Name=" + data.Name);
            this.setProfileFromWs(result);
            console.log('promiseLoginOrRegister:LoginOrRegister:resolve');
            d.resolve(result);
            return d.promise;
          } else {
            console.log('promiseLoginOrRegister:LoginOrRegister:fail');
            d.reject(result);
            return d.promise;
          }
        },
        function(err) {
          console.log('promiseLoginOrRegister:LoginOrRegister:reject:err=' + JSON.stringify(err));
          d.reject(err);
          return d.promise;
        });
    return d.promise;
  }

  this.doPromiseStartAppOnce = function() {
    console.log('doPromiseStartAppOnce');
    // this.setDeviceInfo(ionic.Platform.device());
    var d = $q.defer();
    if (this.alreadyStarting == true) {
      window.localStorage['isFirstEntrance'] = false;
      d.reject(this.alreadyStarting);
      return false;
    }
    this.alreadyStarting = true;
    // TODO: need this but js wasn't finding it, even though I'm merely defining it here
    $scope.startAppCallCount = 1;
    $scope.startErrors = "";
    //console.log('doPromiseStartAppOnce:startAppCallCount='+startAppCallCount);
    window.localStorage['isFirstEntrance'] = false;

    //GAB: works to speed up upcoming but shows error, missing profile info
    //$state.go('app.goals-my');

    return this.promiseStartApp();
    return d.promise;
  }

  // RECURSIVE WORK BACKWARDS
  this.promiseStartApp = function() {
    var Utility = this;
    console.log('promiseStartApp');
    var d = $q.defer();
    var doStart = true;
    console.log('promiseStartApp:call:promiseLoginOrRegister');
    return this.promiseLoginOrRegister()
      .catch(function(err) {
        doStart = false;
        console.log('promiseStartApp:promiseLoginOrRegister:catch:err=' + JSON.stringify(err));
        if (err.data == "") {
          console.log('CATCH:promiseLoginOrRegister:data is empty');
          $scope.startErrors = "data is empty (probably a CORS issue)";
          $scope.startAppCallCount = 100; // exit, this won't be fixed          
        } else {
          $scope.startErrors = JSON.stringify(err);
        }
        return Utility.promiseGetLoginStatus();
      })
      .catch(function(err) {
        $scope.startErrors = JSON.stringify(err);
        console.log('promiseStartApp:promiseGetLoginStatus:catch');
        return Utility.promiseFacebookLogin();
      })
      .finally(function(result) {
        console.log('promiseStartApp:finally:doStart=' + doStart);
        if (doStart) {
          console.log('promiseStartApp:finally:call:startUpcoming');
          this.startUpcoming();
          if (this.varIsSet(this.pushState)) {
            $state.go(this.pushState);
            this.pushState = "";
          }
          this.isAppStarted = true;
          d.resolve(result);
          return d.promise;
        } else {
          // RECURSE
          // TODO, some prob with this startAppCallCount variable
          console.log('promiseStartApp:startAppCallCount=' + $scope.startAppCallCount + ' result=' + result);
          if ($scope.startAppCallCount > 3) {
            alert('startErrors=' + $scope.startErrors);
            d.reject(result);
            return;
          }
          $scope.startAppCallCount++;
          console.log('promiseStartApp:call:promiseStartApp:RECURSE:result=' + result);
          Utility.promiseStartApp();
        }
      });
    return d.promise;
  }

  this.startUpcoming = function() {
    console.log('startUpcoming');
    var getProfileWsr = this.getProfileFromWs();
    console.log('startUpcoming:getProfileWsr=' + getProfileWsr);
    getProfileWsr = JSON.parse(getProfileWsr);
    this.setUserId(getProfileWsr.data.UserId);
    this.updateDeviceId();
    this.setProfileFromWs(getProfileWsr);
    $state.go('app.goals-my');
    if (this.varIsSet(this.pushState)) {
      $state.go(this.pushState);
      Utility.pushState = "";
    }
  }

  this.varIsSet = function(x) {
    if (x == "" || x == null || x == undefined || x == "null" || x == "undefined") {
      return false;
    }
    return true;
  }

  //Update device id  
  this.updateDeviceId = function() {
    var userId = this.getUserId();
    //console.log('updateDeviceId:userId='+userId);
    this.trackEvent("updateDeviceId");
    var did = this.getDeviceId();
    console.log('updateDeviceId:userId' + userId + ' did=' + did);
    Booster.updateDeviceId(userId, did).then(function(response) {
        //Device updated successfully
        console.log('updateDeviceId:response=' + response);
        var test = response;
      },
      function(errorMessage) {
        this.trackError(errorMessage, "updateDeviceId");
        console.log('updateDeviceId:errorMessage=' + this.jStr(errorMessage));
        //this.popup("Error", "Error occured in updateDeviceId");
      }

    );
  };

  this.updateNotifications = function() {
    console.log('updateNotifications');
    Booster.getNotification(this.getUserId()).then(function(result) {
        this.notef.count = result.data.UnReadCount;
        this.notef.list = result.data.Notifications;
      },
      function(errorMessage) {
        this.popup("Error", "Error: updateNotifications");
      });

    this.updateRequestCount();
  }

  this.updateRequestCount = function() {
    //Update request count 
    Booster.getRequestBoostCount(this.getUserId()).then(function(result) {
        this.requestCount = result.data;

      },
      function(errorMessage) {
        this.popup("Error", "Error: updateRequestCount");
      });

  }

  this.trackSendSelfie = function(comment, boostId) // TODO: change to toUserId
    {
      if (use_analytics) analytics.trackEvent('Comment', this.profile.UserId, comment);
      if (this.varIsSet(ga)) ga('send', 'Comment', this.profile.UserId, comment);
    }

  this.trackReview = function(comment, isLovedIt) // TODO: add toUserId
    {
      if (use_analytics) analytics.trackEvent('Comment', this.profile.UserId, comment, isLovedIt);
      ga('send', 'Comment', this.profile.UserId, comment, isLovedIt);
    }

  this.trackEvent = function(act) {
    var d = "x";
    d = this.getDevicePlatform();
    if (d == "Android") d = "A";
    if (d == "iOS") d = "i";

    var page = d + ':' + act;
    console.log('trackEvent:page=' + page);
    if (use_analytics) analytics.trackView(page);

    //gaPlugin.trackPage(nativePluginResultHandler, nativePluginErrorHandler, page);

    console.log('mixpanel.track:' + d + ":" + act);
    mixpanel.track(d + ":" + act);

    if (this.varIsSet(ga)) {
      ga('send', 'screenview', {
        'screenName': act
      });

      //GA 
      ga('send', 'screenview', {
        'appName': 'Booster_WebApp',
        'appId': 'myAppId',
        'appVersion': '1.6.6',
        'appInstallerId': 'myInstallerId',
        'screenName': act
      });

      ga('send', {
        'hitType': 'pageview',
        'page': act,
        'title': act
      });
    }
  }

  this.bulog = function(msg) {
    console.log("ul:" + msg);
    /*
    $cordovaToast.showShortTop('Here is a message').then(function(success) {
        // success
    }, function (error) {
        // error
    });
    */
  }

  this.trackError = function(screenName, errorMessage) {
    this.trackEvent("Error:" + screenName + ':' + errorMessage);
  }

  this.trackUserError = function(act) {
    this.trackEvent("UserError:" + act);
  }

  /*
   ** @result = ws result
   */
  this.setProfileFromWs = function(getProfileWsr) {
    console.log('setProfileFromWs:getProfileWsr=' + getProfileWsr);
    var profileStr;
    try {
      profileStr = JSON.stringify(getProfileWsr);
      console.log('setProfileFromWs:profileStr=' + profileStr);
      window.localStorage['profile'] = profileStr;
    } catch (err) {
      console.log('setProfileFromWs:catch:err=' + err);
    }
    //this.profile.name = this.profile.FirstName + this.profile.LastName;
    //this.profile.points = this.profile.GiveBoostPoint + this.profile.GetBoostPoint;
    console.log('setProfileFromWs:this.profileStr=' + profileStr);
    this.setMixpanelProfile(getProfileWsr);
    if (use_analytics) this.setGaProfile();
  };

  this.setGaProfile = function() {
    var fbid = this.getFBUserId();
    console.log('setGaProfile:fbid=' + fbid);
    analytics.setUserId(fbid);

    var UserId = this.getUserId();
    analytics.addCustomDimension(1, UserId, function(response) {
        console.log('setGaProfile:user-id:addCustomDimension:success:result=' + result);
      },
      function(error) {
        console.log('setGaProfile:user-id:addCustomDimension:fail:result=' + result);
      });

    console.log('setGaProfile:call:addCustomDimension:fbid=' + fbid);
    analytics.addCustomDimension(2, fbid, function(response) {
        console.log('setGaProfile:fbid:addCustomDimension:success:result=' + result);
      },
      function(error) {
        console.log('setGaProfile:fbid:addCustomDimension:fail:result=' + result);
      });

    ga('set', 'fbid', fbid);
    ga('set', 'User ID', UserId);
  }

  this.setMixpanelProfile = function(data) {
    var profile = this.getProfileWsr();
    alert('setMixpanelProfile:profile=' + JSON.stringify(profile));
    profile = JSON.parse(profile);
    this.userInfo = {
      "name": profile.name,
      "fbid": window.localStorage['fbid'],
      "userId": window.localStorage['userId'],
      "points": profile.points,
      "level": profile.level,
      "getBoostPt": profile.getBoostPt,
      "giveBoostPt": profile.giveBoostPt,
      "pic": profile.pic
    }
    this.userDeviceInfo = {
      "device_platform": ionic.Platform.device().platform,
      "dp_version": ionic.Platform.device().version,
      "cordova_version": ionic.Platform.device().cordova,
      "uuid": ionic.Platform.device().uuid,
      "device_model": ionic.Platform.device().model,
      "Platform_connection": ionic.Platform.connection,
      "Platform_platforms": JSON.stringify(ionic.Platform.platforms),
      "Platform_grade": ionic.Platform.grade,
      "Platform_ua": ionic.Platform.ua,
      "isWebView": ionic.Platform.isWebView(),
      "isIPad": ionic.Platform.isIPad()
    }
    mixpanel.people.set(this.userInfo);

    var fbid = this.getFBUserId();
    mixpanel.identify(fbid);
    mixpanel.alias(fbid);

    mixpanel.register_once({
      "userInfo": this.userInfo,
    });

    mixpanel.register({
      "userDeviceInfo": this.userDeviceInfo
    });
  }

  // this.setDeviceInfo = function(myDevice) {
  //   this.setDevicePlatform(myDevice.platform);
  // };

  // this.setDevicePlatform = function(theDp) {
  //   var db = theDp;
  //   try {
  //     window.localStorage['dp'] = dp;
  //   } catch (exc) {
  //     window.localStorage['dp'] = exc;
  //   }
  // }

  this.getDevicePlatform = function() {
    var dp = ionic.Platform.device().platform;
    console.log('getDevicePlatform:dp=' + dp);
    window.localStorage['dp'] = dp;
    return dp;
  }

  this.isDevMode = function() {
    return window.localStorage['isDevMode'];
  }

  this.setDevMode = function(b) {
    window.localStorage['isDevMode'] = b;
  }

  this.getFBUserId = function() {
    return window.localStorage['fbid'];
  };

  this.setFBUserId = function(fbid) {
    //console.log('setFBUserId:fbid='+fbid);  
    window.localStorage['fbid'] = fbid;
  };

  this.getFbFriends = function() {
    var fbFriends = window.localStorage['fbFriends'];
    //console.log('getFbFriends:fbFriends='+fbFriends);             
    return fbFriends;
  };

  this.setFbFriends = function(fbFriends) {
    //console.log('setFbFriends:fbFriends='+this.jStr(fbFriends));             
    window.localStorage['fbFriends'] = this.jStr(fbFriends);
  };

  this.getUserId = function() {
    return 1640;
    return window.localStorage['userId'];
  };

  this.setUserId = function(userId) {
    window.localStorage['userId'] = userId;
  };

  this.getProfileFromWs = function() {
    console.log('getProfileWsr');
    var profile = window.localStorage['profile'];
    console.log('profile=' + profile);
    return profile;
  };

  this.getFbToken = function() {
    var FbToken = window.localStorage['FbToken'];
    console.log('getFbToken:FbToken=' + FbToken);
    return 'CAAI5B3QGPc0BAMOgIxkL0uW93fNlD0E5ZCvpSSZBrXfss55XXMmYILqT8uFyxBHhvRcjvzY4noZAgKBBJHKSkw3EnPNcGAU3PniVA2m8E2KZC2OyLUiUQy4eZB72KQmShFatrcH0kxbm70kPKyefpqtoTepjH1efqFexOy4EMID91cDVIcGTVaHPyjVg4iTZAswcb283IVZCQIdymsqQz1rmmiDQ7ymKrlkbQKvr1xme6P0ZB6jFKhTVe9EDN48ZAGg0ZD';
    return FbToken;
  }

  this.setFbToken = function(FbToken) {
    console.log('setFbToken:FbToken=' + FbToken);
    window.localStorage['FbToken'] = FbToken;
  }

  this.getFbLr = function() {
    var fbLrStr = window.localStorage['fbLr'];
    var fbLr;
    if (this.varIsSet(fbLrStr)) fbLr = JSON.parse(fbLrStr);
    return fbLr;
  }

  this.setFbLr = function(res) {
    console.log('setFbLr:res=' + this.jStr(res));
    //var fbId = res.authResponse.userID;
    var fbId = res.authResponse.id;
    console.log('setFbLr:fbId=' + fbId);
    this.setFBUserId(fbId);
    window.localStorage['fbLr'] = JSON.stringify(res);
  }

  this.getDeviceId = function() {
    var deviceId = window.localStorage['did'];
    console.log('getDeviceId:deviceId=' + deviceId);
    return deviceId;
  };

  this.setDeviceId = function(deviceId) {
    this.myDeviceId = deviceId;
    var before = window.localStorage['did'];
    window.localStorage['did'] = deviceId;
    // If we just acquired a deviceId, where it was null before
    //if(!this.varIsSet(before) && this.varIsSet(deviceId)) 
    //{
    console.log('setDeviceId:updateDeviceId');
    this.updateDeviceId();
    //}
    console.log('setDeviceId:deviceId=' + deviceId);
  };

  this.jStr = function(jsonObj) {
    return JSON.stringify(jsonObj, null, 2);
  }

  this.PENDING = "PENDING";
  this.REQUEST = "REQUEST";
  this.CIMPLITED = "CIMPLITED"
});
