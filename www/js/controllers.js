angular.module('boosterapp.controllers', [])

.controller('AppCtrl', function($scope, $ionicModal, $timeout) {
  // Form data for the login modal
  $scope.loginData = {};

  // Create the login modal that we will use later
  $ionicModal.fromTemplateUrl('templates/login.html', {
    scope: $scope
  }).then(function(modal) {
    $scope.modal = modal;
  });

  // Triggered in the login modal to close it
  $scope.closeLogin = function() {
    $scope.modal.hide();
  };

  // Open the login modal
  $scope.login = function() {
    $scope.modal.show();
  };

  // Perform the login action when the user submits the login form
  $scope.doLogin = function() {
    if (Booster.d() > 5) console.log('Doing login', $scope.loginData);

    // Simulate a login delay. Remove this and replace with your login
    // code if using a login system
    $timeout(function() {
      $scope.closeLogin();
    }, 1000);
  };
})

.controller('BaScrollCtrl', function($scope, $state, $stateParams, Booster, $timeout) {
  // alert('BaScrollCtrl')
  $scope.items = [];
  var counter = 0;
  $scope.loadMore = function() {
    for (var i = 0; i < 50; i++) {
      $scope.items.push({
        id: counter
      });
      counter += 1;
    }
  };
  $scope.loadMore();
})

.controller('InfiniteCtrl', function($scope, $state, $stateParams, Booster, $timeout) {
  $scope.items = [];
  $scope.top = 1;
  $scope.offset = 0;
  $scope.loadMore = function() {
    // $http.get('/more-items').success(function(items) {
    $scope.offset += $scope.top;
    if (Booster.d() > 5) console.log('$scope.offset=' + $scope.offset);
    Booster.getCompletedBoost(1640, $scope.top, $scope.offset).then(function(result) {
      $scope.items.push(result.data);
      $scope.$broadcast('scroll.infiniteScrollComplete');
      $scope.$broadcast('scroll.resize');
    });
  };

  $scope.$on('$stateChangeSuccess', function() {
    $scope.loadMore();
  });
})

.controller('HomeCtrl', function($scope, $state, $stateParams, Booster, $window, $timeout, $q, Utility) {

  var state = $state.$current;

  $scope.screenHeight = $window.innerHeight - 58;
  $scope.screenHeightString = $scope.screenHeight + 'px;';
  // alert($scope.screenHeightString);

  //$('.addBoostBtnImg').css('margin-top', $scope.screenHeight + 'px');
  // $scope.getStyle = function() {
  //     var rv = {
  //         'margin-top': $scope.screenHeightString
  //     };
  //     console.log('rv=' + JSON.stringify(rv));
  //     return rv;
  // }

  $scope.items = [];

  $scope.goalTypesToGet = "all";
  $scope.topN = 1;
  $scope.skipN = 0;
  $scope.haveMore = true;
  $scope.moreDataCanBeLoaded = true;

  // service args
  var getUpcoming = true;
  var withImage = true;
  var who = 0; // enum GetBoostArgs { my, boostees, friends, world }
  var fbToken = Utility.getFbToken();

  var tryAgainCount = 2;
  $scope.stuckTryAgain = tryAgainCount;

  $scope.isShowWorldFilter = false;
  $scope.showWorldFilter = function() {
    return $scope.isShowWorldFilter;
  }

  if (state == 'app.likes') {
    $scope.mytitle = 'Likes';
    who = 0;
  }
  if (state == 'app.goals-my') {
    $scope.mytitle = 'Explore';
    who = 0;
  }
  if (state == 'app.goals-boostees') {
    $scope.mytitle = "Friends's Goals";
    who = 1;
  }
  if (state == 'app.goals-friends') {
    $scope.mytitle = 'Explore';
    $scope.isShowWorldFilter = true;
    who = 2;
  }
  if (state == 'app.goals-world') {
    $scope.mytitle = 'Explore';
    $scope.isShowWorldFilter = true;
    who = 3;
  }

  // checking only from local cache now
  $scope.checkLike = function(item, userId) {
    var boostId = item.Boost.BoostId;
    var myLikeObj = [];
    try {
      var likers = item.ListSelfieCommentsLikes[0].ListSelfieUsersLike._SelfieUsersLike;
      if (likers.length > 0) {
        like = likers.filter(function(val) {
          var isFound = (val.UserId == userId);
          if (isFound) {
            myLikeObj = val;
          }
        });
      }
    } catch (exc) {
      console.log('checkLike:exc=' + exc);
    }
    return myLikeObj;
  }

  // checking only from local cache now
  $scope.getLikeCount = function(i) {
    var item = $scope.items[i];
    var likers = item.ListSelfieCommentsLikes[0].ListSelfieUsersLike;
    return likers.length;
  }

  $scope.getSelfieId = function(i) {
    var item = $scope.items[i];
    var selfieId = item.ListSelfieCommentsLikes[0].ListBoostSelfie[0].SelfieId;
    return selfieId;
  }

  // checking only from local cache now
  $scope.getLikeClass = function(i) {
    var item = $scope.items[i];
    var boostId = item.Boost.BoostId;
    var myLikeObj = $scope.checkLike(item, Utility.getUserId());
    var like = false;
    if (myLikeObj) like = myLikeObj.LikeStatus;
    if (Booster.d() > 5) console.log('getLikeClass:i=' + i + ':boostId=' + boostId + ':like=' + like);
    var likeClasses = [];
    if (like) likeClasses.push('like-button-on');
    else likeClasses.push('like-button-off');
    return likeClasses;
  }

  // updating only local cache now
  $scope.likeClick = function(i) {
    var userId = Utility.getUserId();
    var item = $scope.items[i];
    var boostId = item.Boost.BoostId;
    var myLikeObj = $scope.checkLike(item, userId);
    myLikeObj.LikeStatus = !myLikeObj.LikeStatus;
    var like = myLikeObj.LikeStatus;
    if (like && Booster.d() > 2) console.log('likeClick:i=' + i + ':boostId=' + boostId + ':like=' + like);
    var selfieId = item.ListSelfieCommentsLikes[0].ListBoostSelfie[0].SelfieId;
    // store locally for display
    var likers = item.ListSelfieCommentsLikes[0].ListSelfieUsersLike;
    if (like) {
      likers.push({
        "SelfieId": selfieId,
        "UserId": userId,
        "LikeStatus": like,
        "DateTime": new Date()
      });
    } else {
      var indexOf = likers.indexOf(myLikeObj);
      likers.splice(indexOf, 1);
    }
    // actually update the server (if no network connection, this won't get stored unless we have asych update)
    Booster.likeSelfie(selfieId, like, Utility.getFbToken());
  }

  // add result.data to items, store index in result and BoostId for later caching/matching with selfies
  var processBoostResult = function(result) {
    var rv = [];
    if (Booster.d() > 5) console.log('processBoostResult:result.data.length=' + result.data.length);
    for (var i = 0; i < result.data.length; i++) {
      var di = result.data[i];
      if (Booster.d() > 5000) console.log('i=' + i + ':di=' + Utility.jStr(di));
      $scope.items.push.apply($scope.items, di);
      $scope.stuckTryAgain = tryAgainCount; // reset
      // if(Booster.d() > 5) console.log('processBoostResult:$scope.stuckTryAgain=' + $scope.stuckTryAgain);
      var BoostId = di.BoostId;
      var r = {};
      r["index"] = i;
      r["BoostId"] = BoostId;
      rv.push(r);
      // $scope.BoostIdToIndex[rv.BoostId] = rv.index;
    }
    $scope.$broadcast('scroll.infiniteScrollComplete');
    //$scope.$broadcast('scroll.resize');
    return rv;
  }

  $scope.getAllBoosts = function(who, getUpcoming, withImage, topN, skipN, fbt) {
    var d = $q.defer();
    if (Booster.d() > 5) console.log('skipN=' + skipN);
    Booster.getAllBoosts(who, getUpcoming, withImage, topN, skipN, fbt).then(function(result) {
      if (result.data == "\"END OF LIST\"") {
        alert('getAllBoosts:END OF LIST');
        $scope.haveMore = false;
        d.resolve("END OF BOOST-SELFIES");
        return d.reject("END OF BOOST-SELFIES");
      }
      $scope.items.push.apply($scope.items, result.data);
      d.resolve(result);
      d.return;
    }).then(function(result) {
      //$timeout($scope.delayLoadMore, 1000);
    });
    return d.promise;
  }

  $scope.loadMore = function() {
    if (Booster.d() > 5) console.log('loadMore');
    if ($scope.goalTypesToGet == "all") {
      $scope.getAllBoosts(who, getUpcoming, withImage, $scope.topN, $scope.skipN, fbToken).then(function(result) {
        if (Booster.d() > 5000) console.log('loadMore:success:result=' + Utility.jStr(result));
        var br = processBoostResult(result);
      }).catch(function(exc) {
        $scope.goalTypesToGet = "NONE";
      }).finally(function(result) {});
      $scope.skipN += $scope.topN;
      if (Booster.d() > 5) console.log('2$scope.skipN=' + $scope.skipN);
    }
    if (Booster.d() > 15) console.log('loadMore:$scope.goalTypesToGet=' + $scope.goalTypesToGet);
  }

  // not used, previously, if we got undefined from server, we'd try again after some time
  $scope.delayLoadMore = function() {
    $scope.stuckTryAgain--;
    if (Booster.d() > 50) console.log('delayLoadMore:$scope.stuckTryAgain=' + $scope.stuckTryAgain);
    if ($scope.stuckTryAgain > 0 && $scope.items.length < 3) {
      if (Booster.d() > 10) console.log('delayLoadMore:$scope.items.length=' + $scope.items.length);
      // $scope.$broadcast('scroll.infiniteScrollComplete');
      // $scope.$broadcast('scroll.resize');
      //$scope.loadMore();
    }
  }

  $scope.moreDataCanBeLoaded = function() {
    if (Booster.d() > 5) console.log('moreDataCanBeLoaded:$scope.goalTypesToGet=' + $scope.goalTypesToGet);
    if ($scope.goalTypesToGet == "NONE") {
      return false;
    }
    return true;
  }

  $scope.$on('$stateChangeSuccess', function() {
    if (Booster.d() > 5) console.log("HomeCtrl:$scope.$on('$stateChangeSuccess'");
    // $scope.loadMore();
  });

})

.controller('NotificationsCtrl', function($scope, $stateParams) {

})

.controller('NewGoalSummaryCtrl', function($scope, $stateParams, $ionicPopup, Utility) {
  $scope.boost = {};
  $scope.boost.toFbId;
  $scope.boost.goalText = "Go!";
  $scope.boost.evtDateTime = "12";

  //$state.go('app.')

  if ($scope.boost.toFbId === undefined || $scope.boost.goalText === undefined || $scope.boost.evtDateTime == undefined) {
    $scope.showBoostBtn = false;
  } else {
    $scope.showBoostBtn = true;
  }

  $scope.popUpDateText = "";
  $scope.myPopup;
  $scope.myDatePopup;

  // Check the boost fields to change the send button background 
  $scope.checkValidation = function() {
    if ($scope.boost.toFbId == 0 || $scope.boost.toFbId == "" || $scope.boost.toFbId == null || $scope.boost.toFbId + "" == "undefined" || $scope.boost.comment == "" || $scope.boost.evtDateTime == "" || $scope.boost.evtTime == "") {
      document.getElementById('round-button-circle').style.backgroundImage = "url('img/summary-send-btn-off.png')";
    } else {
      document.getElementById('round-button-circle').style.backgroundImage = "url('img/summary-send-btn-on.png')";
    }
  }

  $scope.openGoalPopup = function() {
    $scope.myPopup = $ionicPopup.show({
      templateUrl: 'templates/popup-goal.tpl.html',
      scope: $scope,
    });
    cordova.plugins.Keyboard.show();
  };

  $scope.closeBtn = function() {
    $scope.myPopup.close();
    cordova.plugins.Keyboard.close();
    $scope.checkValidation();
  };

  $scope.openDatePopup = function() {
    $scope.myDatePopup = $ionicPopup.show({
      templateUrl: 'templates/popup-date.tpl.html',
      scope: $scope,
      // buttons: [
      //   { text: 'Cancel' },
      //   {
      //     text: '<b>Done</b>',
      //     type: 'button-positive',
      //     onTap: function() {
      //       // alert('date send');
      //       $state.go('app.choosebooster');
      //     }
      //   },
      // ]
    });
    // $scope.myDatePopup.then(function(res) {
    //   ionicPopup.close();
    //   $scope.hideKeyboard();

    // });
    // $scope.hideKeyboard();
    cordova.plugins.Keyboard.close();
  }

  $scope.goBtn = function() {
    $scope.goalText = document.getElementById("textarea").value;
    $scope.myPopup.close();
    cordova.plugins.Keyboard.close();
    $scope.showDatetime();
    $scope.checkValidation();
  };

  $scope.showDatetime = function() {
    if (Utility.getDevicePlatform() == "iOS") {
      var options = {
        date: new Date(),
        mode: 'datetime',
        allowOldDates: false,
        backgroundAlpha1: 0.4,
        backgroundAlpha2: 0.4
      };

      datePicker.show(options, function(dto) {
        var sdatetime = dto.format('yyyy-mm-dd HH:MM');
        $scope.boost.evtDateTime = sdatetime;
        $scope.$apply();
      });

      datePicker._dateSelectionCanceled = function() {
        $scope.myDatePopup.close();
        cordova.plugins.Keyboard.close();
      }

      datePicker._dateSelected = function(timeIntervalSince1970) {
        var boostDate = new Date(timeIntervalSince1970 * 1000);
        var sdatetime = boostDate.format('yyyy-mm-dd HH:MM');
        $scope.tempBoostDateTime = sdatetime;
        $scope.myDatePopup.close();
        $scope.checkValidation();
        // $state.go('app.choosebooster');
      }

      datePicker._dateChanged = function(timeIntervalSince1970) {
        var boostDate = new Date(timeIntervalSince1970 * 1000);
        var sdatetime = boostDate.format('yyyy-mm-dd HH:MM');
        // alert(sdatetime);

        $scope.popUpDateText = sdatetime;
      }

      cordova.plugins.Keyboard.close();

    } else {
      var options = {
        date: new Date(),
        mode: 'date',
        allowOldDates: false
      };
      datePicker.show(options, function(dto) {
        var sdate = dto.format('yyyy-mm-dd ');
        if (Booster.d() > 5) console.log('showDatetime:show:sdate=' + sdate);
        $scope.boost.evtDateTime = sdate;
        //$scope.$apply();

        var optionsTime = {
          date: new Date(),
          mode: 'time',
          allowOldDates: true
        };
        datePicker.show(optionsTime, function(dto2) {
          var stime = dto2.format('HH:MM');
          if (Booster.d() > 5) console.log('showDatetime:show:stim=' + stime);
          $scope.boost.evtDateTime += stime;
          $scope.$apply();
        });
      });
    }
    $scope.openDatePopup();
  }

})

.controller('ProfileCtrl', function($scope, $stateParams, Booster, Utility) {
  $scope.FbToken = Utility.getFbToken();

  if ($scope.FbToken == 'undefined') {
    // $scope.FbToken = 'CAAI5B3QGPc0BAANynuUirFsVVY6CmIQGktNtKBJkvaIab1y0YpZCsjhYx5Q3h5Ft92i1o1SqWkzMi4qeuDXwMpH23diOB4w0nzXw1LCoM7TXblq4Ptyx5pzw55T3FoGxuRK1jh3n0IP2BeBx417fUl36Qmt1IQaR2QwbX9WK65LdG7PabwZA6RJ8yLmNKWtGebRhNgC0oRFSvpSNuNkZCjAkzfc9tQZD';
    alert('ProfileCtrl:$scope.FbToken=' + $scope.FbToken);
    Utility.setFbToken($scope.FbToken);
  }

  // alert('ProfileCtrl:call:getProfile');
  // Booster.getProfile(1640).then(function(result) {
  //   alert('getProfile:result=' + result);
  //   $scope.profile = result;
  // }, function(exc) {
  //   alert('getProfile:exc=' + Utility.jStr(exc));
  // });

  // Utility.promiseFacebookLogin().then(function(result) {
  //   $scope.FbToken = Utility.getFbToken();
  // });
})

.controller('InviteCtrl', function($scope, $stateParams) {})

.controller('AboutCtrl', function($scope, $stateParams) {});
